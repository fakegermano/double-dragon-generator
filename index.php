<?php
 	if(isset($_POST["generate"])) {
		/* Simple import and explode of files containing adjectives and nouns (one per line) */
		$a = explode("\n", file_get_contents("adjectives.txt"));
		$n = explode("\n", file_get_contents("nouns.txt"));
		/* Form the double dragon with pseudo-random indexes */
		$doubledragon = $a[array_rand($a)] . " " . $n[array_rand($n)];
	}
?>

<html>
	<head>
		<title> Double Dragon Generator </title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</head>
<body>
	<!-- Boring google analytics tracking, maybe I'll remove this later -->
	<?php include_once("analyticstracking.php") ?>
	<div class="container">
		<div class="row">
			<!-- Lazy grid layout with bootstrap -->
			<div class="col-md-2"></div>
			<div class="col-md-8 text-center">
				<div class="jumbotron">
					<h1> <?php echo $doubledragon ?> </h1>
					<p> This is a simple double dragon name generator made by <a href="http://twitter.com/@balboando_">@balboando_</a> as per Lewis' request on <a href="https://www.youtube.com/watch?v=l8FTPiyZtQw">Double Barrel Jamestown Video</a>.Just click the button below and #TEAMDOUBLEDRAGONFTW </p>
					<!-- Action takes place here, hidden input for POST watch on script -->
					<form action="" method="POST">
						<input type="hidden" name="generate"><br>
						<button type="submit" class="btn btn-primary btn-lg">Double Dragon Generator</button>
					</form>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
</body>
