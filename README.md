# README #

A PHP script to generate Double Dragon names to make Sips_ less awkward at the start of Team DD's videos, and it's quite fun to see the results :100:

### What is this repository for? ###

* Lewis asked for it, I just delivered. Hope you'll enjoy.
* If you wanna contribute to words or improvements to the code, feel free!

### How do I get set up? ###

* Just serve your apache/whatever to the folder that has the files :)

### Contribution guidelines ###

* Just make sure it works :) 

### Who do I talk to? ###

* Me 

### Social stuff ###

* Follow me on twitter (@balboando_), I don't do jack there, but maybe I'll put some coding stuff later on :+1: 